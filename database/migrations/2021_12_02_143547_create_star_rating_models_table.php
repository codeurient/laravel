<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStarRatingModelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('star_rating_models', function (Blueprint $table) {
            $table->id();
            $table->timestamps();

            $table->integer('star_rate')->default(0);

            $table->unsignedBigInteger('post_model_id');
            $table->index('post_model_id');
            $table->foreign('post_model_id')->references('id')->on('post_models')->onDelete('cascade');

            $table->string('ip')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('star_rating_models');
    }
}
