<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCommentModelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comment_models', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->text('message');

            $table->unsignedBigInteger('post_model_id');
            $table->unsignedBigInteger('front_user_id');
            $table->index('post_model_id');
            $table->index('front_user_id');
            $table->foreign('post_model_id')->references('id')->on('post_models')->onDelete('cascade');
            $table->foreign('front_user_id')->references('id')->on('front_users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comment_models');
    }
}
