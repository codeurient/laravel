<?php
use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Admin\MainController;
use App\Http\Controllers\Admin\TagController;
use App\Http\Controllers\Admin\PostController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CommentController as CommentControllerFront;
use App\Http\Controllers\ContactController as ContactControllerFront;
use App\Http\Controllers\StarRatingController as StarRatingControllerFront;
use App\Http\Controllers\EmailVerifyController;
use App\Http\Controllers\PostController as PostControllerFront;
use App\Http\Controllers\SubscribeController as SubscribeControllerFront;
use App\Http\Controllers\CategoryController as CategoryControllerFront;
use App\Http\Controllers\TagController as TagControllerFront;
use App\Http\Controllers\SearchController as SearchControllerFront;
use App\Http\Controllers\TestController;
use App\Http\Controllers\UserController;

use App\Models\User;
use Illuminate\Foundation\Auth\EmailVerificationRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\View;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// front
Route::get('/',                 [PostControllerFront::class, 'index'])->name('home');
Route::post('/subscribe ',      [SubscribeControllerFront::class, 'subscribe'])->name('subscribe');
Route::get('/article/{slug}',   [PostControllerFront::class, 'show'])->name('posts.single');
Route::get('/category/{slug}',  [CategoryControllerFront::class,  'show'])->name('categories.single');
Route::get('/tag/{slug}',       [TagControllerFront::class,       'show'])->name('tags.single');
Route::get('/search',           [SearchControllerFront::class,    'index'])->name('search');
Route::post('/comment/{slug}',  [CommentControllerFront::class,    'saveComment'])->name('comment');
Route::resource('contact-form',  ContactControllerFront::class);


Route::post('/star-rating',  [StarRatingControllerFront::class, 'starRating'])->name('starrating');


// Эти страницы доступны только админу -> 'middleware' => 'admin' - 'namespace' => 'Admin',
Route::group(['prefix' => 'admin', 'middleware' => ['admin', 'verified']], function () {
    Route::get('/',     [MainController::class, 'index'])->name('admin.index');
    Route::resource('/categories',CategoryController::class);
    Route::resource('/tags',TagController::class);
    Route::resource('/posts', PostController::class);
});


Route::group(['middleware' => 'guest'], function () {
    Route::get('/register',  [UserController::class, 'create'])->name('register.create');
    Route::post('/register', [UserController::class, 'store'])->name('register.store');
    Route::get('/login',     [UserController::class, 'loginForm'])->name('login.create');
    Route::post('/login',    [UserController::class, 'login'])->name('login');
});
// К 'logout' будут иметь доступ только авторизованные пользователи '->middleware('auth')'
Route::get('/logout',  [UserController::class, 'logout'])->name('logout')->middleware('auth:web');


Route::get('/email/verify', [EmailVerifyController::class, 'verificationNotice'])->middleware(['auth'])->name('verification.notice');
Route::post('/email/verification-notification', [EmailVerifyController::class, 'verificationLinkSent'])->middleware(['auth', 'throttle:6,1'])->name('verification.send');
Route::get('/email/verify/{id}/{hash}', [EmailVerifyController::class, 'verificationVerify'])->middleware(['auth', 'signed'])->name('verification.verify');













Route::get('/test/create', [TestController::class, 'create'])->name('test.create');
Route::post('/test', [TestController::class, 'store'])->name('test');
Route::get('/testp', [TestController::class, 'testp'])->name('testp')->middleware('auth');




































