<?php
use App\Http\Controllers\AuthController;
use App\Http\Middleware\FrontMiddleware;
use Illuminate\Support\Facades\Route;


Route::middleware([FrontMiddleware::class])->group(function (){
    Route::get('/frontlogin',    [AuthController::class, 'showLoginForm'])->name('frontlogin');
    Route::post('/frontlogin_process',    [AuthController::class, 'login'])->name('frontlogin_process');
    Route::get('/frontregister',    [AuthController::class, 'showRegisterForm'])->name('frontregister');
    Route::post('/frontregister_process',    [AuthController::class, 'register'])->name('frontregister_process');
});
Route::get('/frontlogout',    [AuthController::class, 'logout'])->name('frontlogout')->middleware('auth:front');



























