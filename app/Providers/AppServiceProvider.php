<?php

namespace App\Providers;

use App\Models\CategoryModel;
use App\Models\PostModel;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
            view()->composer('layouts.sidebar', function ($view) {

            if (Cache::has('cats')) {
                $cats = Cache::get('cats');
            } else {
                $cats = CategoryModel::query()->withCount('posts')->orderBy('posts_count', 'desc')->get();
                Cache::put('cats', $cats, 30);
            }

            $view->with('popular_posts', PostModel::query()->orderBy('views', 'desc')->limit(3)->get());
            $view->with('cats', $cats);

        });



    }
}
