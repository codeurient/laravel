<?php

namespace App\Http\Controllers;

use App\Models\PostModel;
use App\Models\StarRatingModel;
use Illuminate\Http\Request;

class StarRatingController extends Controller
{
    public function starRating(Request $request)
    {
        $post_id = $request->post_id;
        $ip = $request->ip();
        $star_rate = $request->userRating;

        $post_check = PostModel::query()->where('id', $post_id)->first();
        if ($post_check) {
            $existing_rating = StarRatingModel::query()->where('ip', $ip)->where('post_model_id', $post_id)->first();
            if ($existing_rating)
            {
                $existing_rating->star_rate = $star_rate;
                $existing_rating->update();
            }
            else
            {
                StarRatingModel::create([
                    'post_model_id' => $post_id,
                    'ip' => $ip,
                    'star_rate' => $star_rate,
                ]);
            }
        }
        $rating = StarRatingModel::query()->where('post_model_id', $post_id)->get();
        $rating_sum = StarRatingModel::query()->where('post_model_id', $post_id)->sum('star_rate');
        if ($rating->count() > 0) {
            $rating_value = round($rating_sum / $rating->count(), PHP_ROUND_HALF_UP);
        } else {
            $rating_value = 0;
        }
//        $rating_value = StarRatingModel::getStarRatingValue($request);
        return response()->json(['newRating' => $rating_value]);
    }

}
