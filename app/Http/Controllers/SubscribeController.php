<?php

namespace App\Http\Controllers;

use App\Models\SubscribeModel;
use Illuminate\Http\Request;

class SubscribeController extends Controller
{
    public function subscribe(Request $request)
    {
        $request->validate([
            'email' => 'email|unique:subscribe_models',
        ]);
        SubscribeModel::create([
            'email' => $request->email,
        ]);
        session()->flash('success', 'Успешно подписались');
        return redirect()->home();
    }
}
