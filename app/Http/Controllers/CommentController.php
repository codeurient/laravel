<?php

namespace App\Http\Controllers;

use App\Models\CommentModel;
use App\Models\PostModel;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    public function saveComment(Request $request, $slug){
        $request->validate([
           'message' => 'required|max:500'
        ]);

        $datas = PostModel::query()->where('slug', $slug)->firstOrFail();
        CommentModel::create([
            'message' => $request->message,
            'post_model_id' => $datas->id,
            //'post_model_id' => $request->post_id,
            'front_user_id' => auth('front')->user()->id,
        ]);
        return redirect()->back();
    }
}
