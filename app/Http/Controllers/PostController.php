<?php

namespace App\Http\Controllers;

use App\Models\FrontUser;
use App\Models\PostModel;
use App\Events\PostHasViewed;
use App\Models\StarRatingModel;
use App\Models\SubscribeModel;
use App\Models\User;
use Illuminate\Http\Request;

class PostController extends Controller
{

    public function index()
    {
        // with('category') - жадный запрос чтоб получилось меньше запросов
        $posts = PostModel::with('category')->orderBy('id', 'desc')->paginate(2);

        return view('posts.index', compact('posts'));
    }

    public function show(Request $request, $slug)
    {
        // Если в view будем писать так - $post->category-title без -  with('category') - то это получится у нас линевый запрос.
        $post = PostModel::query()->where('slug', $slug)->firstOrFail();
        event(new PostHasViewed($post));
//        $post->client_ip = request()->ip();
//        $post->client_visited_date = Date("Y-m-d:H:i:s");
//        $post->increment('views');

        $rating = StarRatingModel::query()->where('post_model_id', $post->id)->get();
        $rating_sum = StarRatingModel::query()->where('post_model_id', $post->id)->sum('star_rate');
        if ($rating->count() > 0) {
            $rating_value = round($rating_sum / $rating->count(), PHP_ROUND_HALF_UP);
        } else {
            $rating_value = 0;
        }
        return view('posts.show', compact('post', 'rating_value', 'rating'));
    }
}
