<?php

namespace App\Http\Controllers;

use App\Models\FrontUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function showLoginForm(){
        return view('auth.login');
    }

    public function showRegisterForm(){
        return view('auth.register');
    }

    public function login(Request $request){
        $data = $request->validate([
            "email" => ["required", "email", "string"],
            "password" => ["required"],
        ]);
        if (auth("front")->attempt($data, true)){
            return redirect()->home();
        }
        return redirect()->route("frontlogin")->with('error', 'Incorrect email or password');
    }

    public function logout(Request $request){
        // В дополнение к вызову logout метода рекомендуется аннулировать сеанс пользователя и повторно сгенерировать его
        auth("front")->logout();
        //$request->session()->invalidate();
        //$request->session()->regenerateToken();
        return redirect()->home();
    }

    public function register(Request $request){
        $data = $request->validate([
            "name" => ["required", "string"],
            "email" => ["required", "email", "string", "unique:front_users,email"],
            "password" => ["required", "confirmed" ],
        ]);
        $users = FrontUser::create([
            'name' => $data["name"],
            'email' => $data["email"],
            'password' => bcrypt($data["password"]),
        ]);
        Auth::guard('front')->login($users);
        return redirect()->home();
    }
}
