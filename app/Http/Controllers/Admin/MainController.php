<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\TagModel;
use App\Models\User;
use Illuminate\Http\Request;

class MainController extends Controller
{
    public function index(Request $request){
/* $tag = new TagModel();
$tag->title = 'Hello world';
$tag->save(); */
        return view('admin.index');
    }
}
