<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\EmailVerificationRequest;
use Illuminate\Http\Request;

class EmailVerifyController extends Controller
{

    // Route для показа страницу, о том что надо подтвердить  аккаунт.
    public function verificationNotice(Request $request){
        if ($request->user()->hasVerifiedEmail() == null){
            return view('user.verify-email');
        } else{
            return redirect()->route('home');
        }
    }


    // Route, о том что отправить сообщение повторно.
    public function verificationLinkSent(Request $request){
        if ($request->user()->hasVerifiedEmail() == true){
            return redirect()->route('home');
        } else{
            $request->user()->sendEmailVerificationNotification();
            return back()->with('message', 'Verification link sent!');
        }
    }


    // Route, для подтверждения.
    public function verificationVerify(EmailVerificationRequest $request){
        $request->fulfill();
        return redirect()->route('home');
    }
}
