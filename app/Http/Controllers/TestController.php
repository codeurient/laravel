<?php

namespace App\Http\Controllers;




use App\Http\Requests\TestRequest;
use App\Models\TestModel;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Validator;

class TestController extends Controller
{

    public function create(Request $request)
    {
        return view('test');
    }

    public function store(TestRequest $request)
    {
        TestModel::query()->create($request->all());
        return redirect()->route('test.create');
    }


    public function testp()
    {
//        $response = Http::get('https://jsonplaceholder.typicode.com/posts', [
//            'title' => 'Steve',
//            'page' => 1,
//        ]);
//        return $response->json();
        return route('testp');
    }


}
