<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        // Auth::check() - проверяет авторизован ли пользователь.
        if (Auth::check() && Auth::user()->is_admin) {
            return $next($request);
        }
        //return redirect()->home();
        return abort('404');

        //return $next($request);
    }



}
