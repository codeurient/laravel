<?php

namespace App\Listeners;

use App\Events\PostHasViewed;
use Illuminate\Cache\RateLimiter;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class Counter
{
    protected $limiter;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(RateLimiter $limiter)
    {
        $this->limiter = $limiter;
    }

    /**
     * Handle the event.
     *
     * @param PostHasViewed $event
     * @param int $hitDelayInMinutes
     * @return void
     */
    public function handle(PostHasViewed $event, $hitDelayInMinutes = 360)
    {
        $key = sha1(request()->fullUrl() . '|' . request()->ip());

        if (!$this->limiter->tooManyAttempts($key, 1)){
            $event->post->increment('views');
        }
        $this->limiter->hit($key, $hitDelayInMinutes);

    }
}
