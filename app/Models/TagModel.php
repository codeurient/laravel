<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class TagModel extends Model
{
    use HasFactory;
    use Sluggable;

    protected $fillable = [
        'title',
        'description'
    ];

    public function posts()
    {
        // теги (TagModel) принадлежат (belongsToMany) к постам (PostModel::class).
        return $this->belongsToMany(PostModel::class, 'post_tag', 'tag_id', 'post_id');
    }

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable() : array
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }
}
