<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class PostModel extends Model
{
    use HasFactory;
    use Sluggable;

    protected $fillable = ['title', 'description', 'content', 'category_id', 'thumbnail'];

    // Посты (PostModel) принадлежат (belongsToMany) к тегам (TagModel::class).
    public function tags()
    {
        // ->withTimestamps(); - чтоб сохранить время в БД когда сохранятеся  'post_id' и 'tag_id'
        return $this->belongsToMany(TagModel::class, 'post_tag', 'post_id', 'tag_id')->withTimestamps();
    }

    // Каждый пост (PostModel) можеть принадлежать к одной (belongsTo) категории (CategoryModel::class).
    public function category()
    {
        return $this->belongsTo(CategoryModel::class, 'category_id', 'id');
    }

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable() : array
    {
        return [
            // Поля  'slug' => заполняется с поли 'source' => 'title'
            'slug' => [

                'source' => 'title'

            ]
        ];
    }


    public static function uploadImage(Request $request, $image = null)
    {
        //dd($image); - "images/2021-10-14/G6ej1rAco32D61jl9A4MPDY3UXinM2K4Iv2pEEYV.jpg"
        if ($request->hasFile('thumbnail')) {
            if ($image) {
                Storage::delete($image);
            }
            $folder = date('Y-m-d');
            return $request->file('thumbnail')->store("images/{$folder}");
        }

        /*
        // Этот 'elseif' и тот 'if' который находится в PostController выполняют то же самое - if ($file = PostModel::uploadImage($request, $post->thumbnail)){$data['thumbnail'] = $file;}

        elseif ($image) {
            return $image;
        }

        */
        return null;
    }

    public function getImage()
    {
        if (!$this->thumbnail) {
            return asset("no-image.png");
        }
        return asset("uploads/{$this->thumbnail}");
    }

    public function getPostDate()
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $this->created_at)->format('d F, Y');
    }

    public function scopeLike($query, $s)
    {
        return $query->where('title', 'LIKE', "%{$s}%");
    }

    public function comments(){
        return $this->hasMany(CommentModel::class, 'post_model_id', 'id')->orderBy('created_at', 'desc');
    }

    public function starrating(){
        return $this->hasMany(StarRatingModel::class, 'post_model_id', 'id');
    }
}
