<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CommentModel extends Model
{
    use HasFactory;

    protected $fillable = [
        'message',
        'post_model_id',
        'front_user_id',
        ];

    public function frontuser(){
        return $this->belongsTo(FrontUser::class, 'front_user_id', 'id');
    }

    public function post(){
        return $this->belongsTo(PostModel::class, 'post_model_id', 'id');
    }
}
