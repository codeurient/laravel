<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class CategoryModel extends Model
{
    use HasFactory;
    use Sluggable;

    protected $fillable = ['title'];

    // Каждыя категория (CategoryModel) может иметь много (hasMany) постов (PostModel::class).
    // Например категория А имеет 1,2,3,4,5 и т.д постов.
    public function posts()
    {
        return $this->hasMany(PostModel::class, 'category_id', 'id');
    }

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable() : array
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }
}
