<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class StarRatingModel extends Model
{
    use HasFactory;

    protected $fillable = [
        'post_model_id',
        'ip',
        'star_rate',
    ];

    public function post(){
        return $this->belongsTo(PostModel::class, 'post_model_id', 'id');
    }
}
