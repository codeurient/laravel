@extends('layouts.layout')

@section('title', 'Markedia - Marketing Blog Template :: ' . $post->title)

@section('content')
    <div class="page-wrapper" style="padding: 1rem 1rem">
        <div class="blog-title-area" style="margin-bottom: 1rem">
            <ol class="breadcrumb hidden-xs-down">
                <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                <li class="breadcrumb-item"><a
                        href="{{ route('categories.single', ['slug' => $post->category->slug]) }}">{{ $post->category->title }}</a>
                </li>
                <li class="breadcrumb-item active">{{ $post->title }}</li>
            </ol>

            {{--<span class="color-yellow"><a href="{{ route('categories.single', ['slug' => $post->category->slug]) }}" >{{ $post->category->title }}</a></span>--}}




            <form action="#" class="form form_margin">
                {{--<div class="form__item">
                    <div class="form__label">Простой рейтинг (без JS)</div>
                    <div class="simple-rating">
                        <div class="simple-rating__items">
                            <input id="simple-rating__5" type="radio" class="simple-rating__item" name="simple-rating" value="5">
                            <label for="simple-rating__5" class="simple-rating__label"></label>
                            <input id="simple-rating__4" type="radio" class="simple-rating__item" name="simple-rating" value="4">
                            <label for="simple-rating__4" class="simple-rating__label"></label>
                            <input id="simple-rating__3" type="radio" class="simple-rating__item" name="simple-rating" value="3">
                            <label for="simple-rating__3" class="simple-rating__label"></label>
                            <input id="simple-rating__2" type="radio" class="simple-rating__item" name="simple-rating" value="2">
                            <label for="simple-rating__2" class="simple-rating__label"></label>
                            <input id="simple-rating__1" type="radio" class="simple-rating__item" name="simple-rating" value="1">
                            <label for="simple-rating__1" class="simple-rating__label"></label>
                        </div>
                    </div>
                </div>--}}



{{--                <div class="form__item">
                    <div class="form__label">Точный рейтинг (JS)</div>
                    <div class="rating rating_set">
                        <div class="rating__body">
                            <div class="rating__active"></div>
                            <div class="rating__items">
                                <input type="radio" class="rating__item" value="1" name="rating">
                                <input type="radio" class="rating__item" value="2" name="rating">
                                <input type="radio" class="rating__item" value="3" name="rating">
                                <input type="radio" class="rating__item" value="4" name="rating">
                                <input type="radio" class="rating__item" value="5" name="rating">
                            </div>
                        </div>
                        <div class="rating__value">3.6</div>
                    </div>
                </div>
                <button type="submit" class="form__btn btn">Отправить</button>--}}
            </form>


            <form method="POST">
                <input type="hidden" id="token" value="{{ @csrf_token() }}">
                <input type="hidden" id="post_id" name="post_id" value="{{ $post->id }}">
            <div class="form mt-2 mb-2">
                <div data-ajax="true" class="rating rating_set">
                    <div class="rating__body">
                        <div class="rating__active"></div>
                        <div class="rating__items">
                            <input type="radio" class="rating__item" value="1" name="rating">
                            <input type="radio" class="rating__item" value="2" name="rating">
                            <input type="radio" class="rating__item" value="3" name="rating">
                            <input type="radio" class="rating__item" value="4" name="rating">
                            <input type="radio" class="rating__item" value="5" name="rating">
                        </div>
                    </div>

                    <div class="rating__value">
                        @if($rating->count() > 0)
                            {{ $rating_value }}
                        @else
                            0
                        @endif
                    </div>
                </div>
            </div>
            </form>



            <h3>{{ $post->title }}</h3>

            <div class="blog-meta big-meta">
                <small>{{ $post->getPostDate() }}</small>
                <small><i class="fa fa-eye"></i> {{ $post->views }}</small>
            </div><!-- end meta -->

        </div><!-- end title -->

        <div class="single-post-media">
            <img src="{{ $post->getImage() }}" alt="" class="img-fluid">
        </div><!-- end media -->

        <div class="blog-content">
            {!! $post->content !!}
        </div><!-- end content -->

        <div class="blog-title-area">
            @if($post->tags->count())
                <div class="tag-cloud-single">
                    <span>Tags</span>
                    @foreach($post->tags as $tag)
                        <small><a href="{{ route('tags.single', ['slug' => $tag->slug]) }}" title="">{{ $tag->title }}</a></small>
                    @endforeach
                </div><!-- end meta -->
            @endif

            <div class="post-sharing">
                <ul class="list-inline">
                    <li>
                        <a href="#" class="fb-button btn btn-primary" id="facebook-btn" target="_blank"><i class="fa fa-facebook"></i>
                            <span class="down-mobile">Share on Facebook</span>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="tw-button btn btn-primary" id="twitter-btn" target="_blank"><i class="fa fa-twitter"></i>
                            <span class="down-mobile">Tweet on Twitter</span>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="gp-button btn btn-primary" id="gplus-btn" target="_blank"><i class="fa fa-google-plus"></i></a>
                    </li>
{{--                    <li>--}}
{{--                        <a style="display: none" href="#" class="sh-button btn btn-info" id="shareBtn" target="_blank" ><i class="fa fa fa-share text-white" aria-hidden="true"></i>--}}
{{--                            <span class="down-mobile">Share</span>--}}
{{--                        </a>--}}
{{--                    </li>--}}
                </ul>
            </div><!-- end post-sharing -->
        </div><!-- end title -->

        <div class="row">
            <div class="col-lg-12">
                <div class="banner-spot clearfix">
                    <div class="banner-img">
                        <img src="" alt="" class="img-fluid">
                    </div><!-- end banner-img -->
                </div><!-- end banner -->
            </div><!-- end col -->
        </div><!-- end row -->

        <hr class="invis1">

        <div class="custombox authorbox clearfix">
            <h4 class="small-title">About author</h4>
            <div class="row">
                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                    <img src="" alt="" class="img-fluid rounded-circle">
                </div><!-- end col -->

                <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
                    <h4><a href="#">Jessica</a></h4>
                    <p>Quisque sed tristique felis. Lorem <a href="#">visit my website</a> amet, consectetur adipiscing
                        elit. Phasellus quis mi auctor, tincidunt nisl eget, finibus odio. Duis tempus elit quis risus
                        congue feugiat. Thanks for stop Markedia!</p>

                    <div class="topsocial">
                        <a href="#" data-toggle="tooltip" data-placement="bottom" title="Facebook"><i
                                class="fa fa-facebook"></i></a>
                        <a href="#" data-toggle="tooltip" data-placement="bottom" title="Youtube"><i
                                class="fa fa-youtube"></i></a>
                        <a href="#" data-toggle="tooltip" data-placement="bottom" title="Pinterest"><i
                                class="fa fa-pinterest"></i></a>
                        <a href="#" data-toggle="tooltip" data-placement="bottom" title="Twitter"><i
                                class="fa fa-twitter"></i></a>
                        <a href="#" data-toggle="tooltip" data-placement="bottom" title="Instagram"><i
                                class="fa fa-instagram"></i></a>
                        <a href="#" data-toggle="tooltip" data-placement="bottom" title="Website"><i
                                class="fa fa-link"></i></a>
                    </div><!-- end social -->

                </div><!-- end col -->
            </div><!-- end row -->
        </div><!-- end author-box -->

        <hr class="invis1">

        <hr class="invis1">

        <div class="custombox clearfix">
            <h4 class="small-title">{{ count($post->comments) }} Comments</h4>
            <div class="row">
                <div class="col-lg-12">
                    <div class="comments-list">

                        @if($post->comments)
                            @foreach($post->comments as $comment)
                                <div class="media">
                                    <div class="media-body">
                                        <h4 class="media-heading user_name">{{ $comment->frontuser->name }}</h4>
                                        <p>{{ $comment->message }}</p>
                                        {{--<a href="#" class="btn btn-primary btn-sm">Reply</a>--}}
                                    </div>
                                </div>
                                <hr>
                            @endforeach
                        @endif

                    </div>
                </div><!-- end col -->
            </div><!-- end row -->
        </div><!-- end custom-box -->

        <hr class="invis1">



        @auth("front")
            <div class="custombox clearfix">
                <h4 class="small-title">Leave a Reply</h4>
                <div class="row">
                    <div class="col-lg-12">
                        <form class="form-wrapper" method="post" action="{{ url('comment/'.Str::slug($post->title)) }}">
                            {{--<form class="form-wrapper" method="post" action="{{ route('comment') }}">--}}
                            @csrf
                            {{--<input type="hidden" name="post_id" value="{{ $post->id }}">--}}
                            <textarea          name="message" class="form-control" placeholder="Your comment"></textarea>
                            <button type="submit" class="btn btn-primary" style="cursor: pointer;">Submit Comment</button>
                        </form>
                    </div>
                </div>
            </div>
        @endauth

        @guest("front")
            <div class="custombox clearfix">
                <div class="row">
                    <div class="col-lg-12">
                        <h4 class="small-title">Login please to leave your comment</h4>
                    </div>
                </div>
            </div>
        @endguest


    </div><!-- end page-wrapper -->

@endsection
