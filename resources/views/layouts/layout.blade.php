<!DOCTYPE html>
<html lang="en">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>@yield('title')</title>
<meta name="keywords" content="">
<meta name="description" content="">
<meta name="author" content="">
<link rel="shortcut icon" href="{{ asset('assets/front/images/favicon.ico') }}" type="image/x-icon" />
<link rel="apple-touch-icon" href="{{ asset('assets/front/images/apple-touch-icon.png') }}">
<link href="https://fonts.googleapis.com/css?family=Roboto+Slab:400,700" rel="stylesheet">

<link href="{{ asset('assets/front/css/front.css') }}" rel="stylesheet" >

<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->

<style>
    /* -------------- */
    /* ---  С JS  --- */
    /* -------------- */
    .rating {
        display: flex;
        align-items: flex-end;
        font-size: 20px;
        line-height: 0.75;
        transition: opacity 0.3s ease 0s;
        -webkit-transition: opacity 0.3s ease 0s;
        -moz-transition: opacity 0.3s ease 0s;
        -ms-transition: opacity 0.3s ease 0s;
        -o-transition: opacity 0.3s ease 0s;
    }
    .rating.rating_sending{
        opacity: 0.2;
    }
    .rating.rating_set .rating__active,
    .rating.rating_set .rating__item {
        cursor: pointer;
    }
    .rating__body {
        position: relative;
    }
    .rating__body::before {
        content: "★★★★★";
        display: block;
        color: #000;
    }
    .rating__active {
        position: absolute;
        width: 0;
        height: 100%;
        top: 0;
        left: 0;
        overflow: hidden;
    }
    .rating__active::before {
        content: "★★★★★";
        position: absolute;
        width: 100%;
        height: 100%;
        top: 0;
        left: 0;
        color: #ffd300;
    }
    .rating__items {
        display: flex;
        position: absolute;
        width: 100%;
        height: 100%;
        top: 0;
        left: 0;
    }
    .rating__item {
        flex: 0 0 20%;
        height: 100%;
        opacity: 0;
    }
    .rating__value {
        font-size: 70%;
        line-height: 1;
        padding: 0 0 0 10px;
    }
     @media (max-width: 590px) {
         .form{
            text-align: center;
             display: inline-block;
        }
     }
</style>


</head>
<body>

<div id="wrapper">

    @include('layouts.navbar')

    @yield('header')

    <section class="section lb @if(!Request::is('/')) m3rem @endif">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">

                    @yield('content')

                </div><!-- end col -->

                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                    @include('layouts.sidebar')
                </div><!-- end col -->
            </div><!-- end row -->
        </div><!-- end container -->
    </section>

    <footer class="footer">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                    <div class="widget">
                        <h2 class="widget-title">Recent Posts</h2>
                        <div class="blog-list-widget">
                            <div class="list-group">
                                <a href="marketing-single.html" class="list-group-item list-group-item-action flex-column align-items-start">
                                    <div class="w-100 justify-content-between">
                                        <img src="/assets/front/upload/small_04.jpg" alt="" class="img-fluid float-left">
                                        <h5 class="mb-1">5 Beautiful buildings you need to before dying</h5>
                                        <small>12 Jan, 2016</small>
                                    </div>
                                </a>

                                <a href="marketing-single.html" class="list-group-item list-group-item-action flex-column align-items-start">
                                    <div class="w-100 justify-content-between">
                                        <img src="/assets/front/upload/small_05.jpg" alt="" class="img-fluid float-left">
                                        <h5 class="mb-1">Let's make an introduction for creative life</h5>
                                        <small>11 Jan, 2016</small>
                                    </div>
                                </a>

                                <a href="marketing-single.html" class="list-group-item list-group-item-action flex-column align-items-start">
                                    <div class="w-100 last-item justify-content-between">
                                        <img src="/assets/front/upload/small_06.jpg" alt="" class="img-fluid float-left">
                                        <h5 class="mb-1">Did you see the most beautiful sea in the world?</h5>
                                        <small>07 Jan, 2016</small>
                                    </div>
                                </a>
                            </div>
                        </div><!-- end blog-list -->
                    </div><!-- end widget -->
                </div><!-- end col -->

                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                    <div class="widget">
                        <h2 class="widget-title">Popular Posts</h2>
                        <div class="blog-list-widget">
                            <div class="list-group">
                                <a href="marketing-single.html" class="list-group-item list-group-item-action flex-column align-items-start">
                                    <div class="w-100 justify-content-between">
                                        <img src="/assets/front/upload/small_01.jpg" alt="" class="img-fluid float-left">
                                        <h5 class="mb-1">Banana-chip chocolate cake recipe with customs</h5>
                                        <span class="rating">
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                            </span>
                                    </div>
                                </a>

                                <a href="marketing-single.html" class="list-group-item list-group-item-action flex-column align-items-start">
                                    <div class="w-100 justify-content-between">
                                        <img src="/assets/front/upload/small_02.jpg" alt="" class="img-fluid float-left">
                                        <h5 class="mb-1">10 practical ways to choose organic vegetables</h5>
                                        <span class="rating">
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                            </span>
                                    </div>
                                </a>

                                <a href="marketing-single.html" class="list-group-item list-group-item-action flex-column align-items-start">
                                    <div class="w-100 last-item justify-content-between">
                                        <img src="/assets/front/upload/small_03.jpg" alt="" class="img-fluid float-left">
                                        <h5 class="mb-1">We are making homemade ravioli, nice and good</h5>
                                        <span class="rating">
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                            </span>
                                    </div>
                                </a>
                            </div>
                        </div><!-- end blog-list -->
                    </div><!-- end widget -->
                </div><!-- end col -->

                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                    <div class="widget">
                        <h2 class="widget-title">Popular Categories</h2>
                        <div class="link-widget">
                            <ul>
                                <li><a href="#">Marketing <span>(21)</span></a></li>
                                <li><a href="#">SEO Service <span>(15)</span></a></li>
                                <li><a href="#">Digital Agency <span>(31)</span></a></li>
                                <li><a href="#">Make Money <span>(22)</span></a></li>
                                <li><a href="#">Blogging <span>(66)</span></a></li>
                                <li><a href="#">Entertaintment <span>(11)</span></a></li>
                                <li><a href="#">Video Tuts <span>(87)</span></a></li>
                            </ul>
                        </div><!-- end link-widget -->
                    </div><!-- end widget -->
                </div><!-- end col -->
            </div><!-- end row -->

            <div class="row">
                <div class="col-md-12 text-center">
                    <br>
                    <br>
                    <div class="copyright">&copy; Markedia. Design: <a href="http://html.design">HTML Design</a>.</div>
                </div>
            </div>
        </div><!-- end container -->
    </footer><!-- end footer -->

    <div class="dmtop" style="overflow: hidden">Scroll to Top</div>

</div><!-- end wrapper -->


<script>
    "use strict"
    const ratings = document.querySelectorAll('.rating');
    if(ratings.length > 0) {
        initRatings();
    }
    async function initRatings() {
        let ratingActive, ratingValue;
        for (let index = 0; index < ratings.length; index++) {
            const rating = ratings[index];
            initRatings(rating);
        }
        function initRatings(rating) {
            initRatingVars(rating);
            setRatingActiveWidth();
            if (rating.classList.contains('rating_set')) {
                setRating(rating);
            }
        }
        function initRatingVars(rating) {
            ratingActive = rating.querySelector('.rating__active');
            ratingValue = rating.querySelector('.rating__value');
        }
        function setRatingActiveWidth(index = ratingValue.innerHTML) {
            const ratingActiveWidth = index / 0.05;
            ratingActive.style.width = `${ratingActiveWidth}%`;
        }
        function setRating(rating) {
            const ratingItems = rating.querySelectorAll('.rating__item');
            for (let index = 0; index < ratingItems.length; index++) {
                const ratingItem = ratingItems[index];
                ratingItem.addEventListener("mouseenter", function (e) {
                    initRatingVars(rating);
                    setRatingActiveWidth(ratingItem.value);
                });
                ratingItem.addEventListener("mouseleave", function (e) {
                    setRatingActiveWidth();
                });
                ratingItem.addEventListener("click", function (e) {
                    initRatingVars(rating);
                    if (rating.dataset.ajax) {
                        setRatingValue(ratingItem.value, rating);
                    } else {
                        ratingValue.innerHTML = index + 1;
                        setRatingActiveWidth();
                    }
                });
            }
        }
        async function setRatingValue(value, rating) {
            if (!rating.classList.contains('rating_sending')){
                rating.classList.add('rating_sending');
                let token = document.querySelector('#token');
                let post_id = document.querySelector('#post_id');
                let response = await fetch('{{ route('starrating') }}', {
                    method: 'POST',
                    body: JSON.stringify({
                        _token: token.value,
                        post_id: post_id.value,
                        userRating: value
                    }),
                    headers: {
                        'content-type': 'application/json'
                    }
                });
                //console.log(response);
                if (response.ok) {
                    const result = await response.json();
                    const newRating = result.newRating;
                    ratingValue.innerHTML = newRating;
                    setRatingActiveWidth();
                    rating.classList.remove('rating_sending');
                } else {
                    alert("Ошибка");
                    rating.classList.remove('rating_sending');
                }
            }
        }
    }
</script>

<script type="text/javascript">
    // Social Share links.
    const facebookBtn = document.getElementById('facebook-btn');
    const gplusBtn = document.getElementById('gplus-btn');
    const twitterBtn = document.getElementById('twitter-btn');
    //const socialLinks = document.getElementById('social-links');
    // posturl posttitle
    let postUrl = encodeURI(document.location.href);
    //console.log(postUrl);
    let postTitle = encodeURI('{{$post->title}}');
    let postImg = encodeURI('{{$post->thumbnail}}');
    //console.log(postImg);
    facebookBtn.setAttribute("href",`https://www.facebook.com/sharer.php?u=${postUrl}&text=${postTitle}&img=${postImg}`);
    twitterBtn.setAttribute("href", `https://twitter.com/share?url=${postUrl}`);
    gplusBtn.setAttribute("href",`https://plus.google.com/share?url=${postUrl}`);

    /*// Button - https://github.com/subhadipghorui/laravel-7-blog-project/blob/master/resources/views/post.blade.php
    const shareBtn = document.getElementById('shareBtn');
    if(navigator.share){
        shareBtn.style.display = 'block';
        socialLinks.style.display = 'none';
        shareBtn.addEventListener('click', ()=>{
            navigator.share({
                title: postTitle,
                url:postUrl
            }).then((result) => {
                alert('Thank You for Sharing.')
            }).catch((err) => {
                console.log(err);
            });
        });
    }else{
    }*/
</script>
<script src="{{ asset('assets/front/js/front.js') }}"></script>

<script>

</script>
</body>
</html>
