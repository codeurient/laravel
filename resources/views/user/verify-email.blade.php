<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<h1>Подтвердите адрес электронной почты пожалуйста</h1>

<form method="POST" action="{{ route('verification.send') }}">
    @csrf

    <div class="row">
        <!-- /.col -->
        <div class="col-4 offset-8">
            <button type="submit" class="btn btn-primary btn-block">Отправить повторно</button>
        </div>
        <!-- /.col -->
    </div>
</form>

</body>
</html>
