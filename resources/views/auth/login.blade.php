<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel c нуля</title>

    <link href="https://unpkg.com/tailwindcss@2.0.3/dist/tailwind.min.css" rel="stylesheet">
</head>
<body>
<div class="h-screen bg-white flex flex-col space-y-10 justify-center items-center">
    <div class="bg-white w-96 shadow-xl rounded p-5">
        <a href="{{ route('home') }}"><img src="{{ asset('uploads/images/slack.png') }}" alt="logo" style="width:38px;height:38px;"></a>
        <h1 class="mt-2 text-3xl font-medium">Вход</h1>

        @if(session()->has('error'))
            <div style="color: red; margin-top: 5px">
                {{ session('error') }}
            </div>
        @endif

        <form method="post" action="{{ route("frontlogin_process") }}" class="space-y-5 mt-5">
            @csrf

            <label>
                <input name="email" type="text" class="w-full h-12 border border-gray-500 rounded px-3 @error('email') border-red-500 @enderror" placeholder="Email" />
            </label>
                @error('email')
                    <p class="text-red-500">{{ $message }}</p>
                @enderror

            <p class="text-red-500"></p>

            <label>
                <input name="password" type="password" class="w-full h-12 border border-gray-800 rounded px-3 @error('password') border-red-500 @enderror" placeholder="Пароль" />
            </label>
                @error('password')
                    <p class="text-red-500">{{ $message }}</p>
                @enderror

            <div>
                <a href="#" class="font-medium text-blue-900 hover:bg-blue-300 rounded-md p-2">Забыли пароль?</a>
            </div>

            <div>
                <a href="{{ route('frontregister') }}" class="font-medium text-blue-900 hover:bg-blue-300 rounded-md p-2">Регистрация</a>
            </div>

            <button type="submit" class="text-center w-full bg-blue-900 rounded-md text-white py-3 font-medium">Войти</button>
        </form>
    </div>
</div>
</body>
</html>

